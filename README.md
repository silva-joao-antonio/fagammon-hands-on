Para construir a imagem:

`
docker build -t hands_on .
`

Para executar

`
docker run --net=host -it --rm -v /hands:/home/joao-a-silva hands_on jupyter notebook --allow-root
`

